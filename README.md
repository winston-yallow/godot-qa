# Godot QA

Small endless idle clicker game where the player sorts out builds with bugs.

## :warning: Attention
This was created in just one day. My focus was on fininshing this, not on writing good or reusable code.
Feel free use it as you like (in compliance with the licenses of course), but keep in mind that there
may be better ways to achieve some things.

## License
The whole project is MIT licensed except for the OFL licensed font.
