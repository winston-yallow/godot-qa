extends Area2D


const BuildItem := preload('res://src/build_item/BuildItem.gd')

export var max_input_delay := 0.25  # 250 ms
export var location_node_accepted: NodePath
export var location_node_rejected: NodePath
export var stats_node: NodePath

onready var _location_accpeted: Vector2 = get_node(location_node_accepted).position
onready var _location_rejected: Vector2 = get_node(location_node_rejected).position
onready var _stats: Label = get_node(stats_node)
onready var _anim: AnimationPlayer = $AnimationPlayer
onready var _light_green: Sprite = $light_green
onready var _light_red: Sprite = $light_red

var stat_builds_delivered := 0
var stat_bugs_delivered := 0
var stat_builds_wrongly_destroyed := 0

var _reject := false
var _time_since_last_reject_press := 0.0


func _ready() -> void:
    # warning-ignore:return_value_discarded
    connect('area_entered', self, '_on_area_entered')


func _process(delta: float) -> void:
    _time_since_last_reject_press += delta
    if Input.is_action_pressed('reject_build'):
        _time_since_last_reject_press = 0.0
    _reject = _time_since_last_reject_press < max_input_delay
    _light_green.visible = not _reject
    _light_red.visible = _reject
    
    var ratio := 0.0
    if stat_builds_delivered != 0:
        ratio = float(stat_bugs_delivered) / float(stat_builds_delivered)
    _stats.text = 'Godot builds published: %s\nBugs in production: %s\nBug-Ratio: %.3f' % [
        stat_builds_delivered,
        stat_bugs_delivered,
        ratio
    ]


func _on_area_entered(other: Node) -> void:
    if other is BuildItem:
        if _reject:
            _anim.play('right')
            other.sort_to(_location_rejected)
            if other.bug_count == 0:
                stat_builds_wrongly_destroyed += 1
        else:
            _anim.play('left')
            other.sort_to(_location_accpeted)
            stat_builds_delivered += 1
            stat_bugs_delivered += other.bug_count
