extends Area2D


const BuildItem := preload('res://src/build_item/BuildItem.gd')

onready var _label: Label = $Label


func _ready() -> void:
    # warning-ignore:return_value_discarded
    connect('area_entered', self, '_on_area_entered')


func _on_area_entered(other: Node) -> void:
    if other is BuildItem:
        _label.text = 'Bugs: %s' % other.bug_count
