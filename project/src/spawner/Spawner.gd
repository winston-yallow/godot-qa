extends Node2D


const BuildItemScene := preload('res://src/build_item/BuildItem.tscn')

export var bug_probability := 0.5
export var max_bug_count := 8.0
export(float, EASE) var random_distribution := 2.0


func _ready() -> void:
    randomize()
    # warning-ignore:return_value_discarded
    $Timer.connect('timeout', self, '_spawn_build')


func _spawn_build() -> void:
    var new_build := BuildItemScene.instance()
    new_build.bug_count = round((randf() * (max_bug_count - 1)) + 1.0) if randf() > bug_probability else 0
    new_build.position = position
    new_build.z_index = z_index
    get_tree().current_scene.add_child(new_build)
