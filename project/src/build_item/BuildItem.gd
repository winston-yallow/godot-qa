extends Area2D


enum STATE { MOVING, SORT, TWEEN }

export var move_speed := 64.0
export var sort_speed := 200.0
export var fly_speed := 400.0
export var rotation_speed_max := 6.0
export var rotation_speed_min := 0.2
export var rotation_damp := 2.0
export var bug_count := 0

onready var _tween: Tween = $Tween
onready var _sprite: Sprite = $Sprite

var _current_state: int = STATE.MOVING
var _sort_target: Vector2

var _current_rotation_speed := 0.0
var _target_rotation_speed := 0.0

var _tween_velocity := Vector2.ZERO
var _tween_rotation := 0.0


func _physics_process(delta: float) -> void:
    match _current_state:
        
        STATE.MOVING:
            position += Vector2.UP * move_speed * delta
        
        STATE.SORT:
            position += position.direction_to(_sort_target) * sort_speed * delta
            rotation += _current_rotation_speed * delta
            _current_rotation_speed = lerp(
                _current_rotation_speed,
                _target_rotation_speed,
                rotation_damp * delta
            )
        
        STATE.TWEEN:
            position += _tween_velocity * delta
            rotation += _tween_rotation * delta


func sort_to(target: Vector2) -> void:
    _current_state = STATE.SORT
    _sort_target = target
    _sprite.frame = 1 if bug_count == 0 else 2
    
    var rotation_direction := sign(position.direction_to(target).x)
    _current_rotation_speed = rotation_direction * rotation_speed_max
    _target_rotation_speed = rotation_direction * rotation_speed_min


func reject() -> void:
    _current_state = STATE.TWEEN
    # warning-ignore:return_value_discarded
    _tween.interpolate_property(
        self,
        '_tween_velocity',
        position.direction_to(_sort_target) * sort_speed,
        Vector2.DOWN * fly_speed,
        0.6,
        Tween.TRANS_CUBIC,
        Tween.EASE_IN
    )
    # warning-ignore:return_value_discarded
    _tween.interpolate_property(
        self,
        '_tween_rotation',
        _current_rotation_speed,
        0.0,
        0.3,
        Tween.TRANS_CUBIC,
        Tween.EASE_IN
    )
    # warning-ignore:return_value_discarded
    _tween.interpolate_property(
        self,
        'modulate',
        Color.white,
        Color(0, 0, 0, 0),
        0.3,
        Tween.TRANS_CUBIC,
        Tween.EASE_IN,
        0.3
    )
    # warning-ignore:return_value_discarded
    _tween.start()
    # warning-ignore:return_value_discarded
    _tween.connect('tween_all_completed', self, 'queue_free')


func accept() -> void:
    _current_state = STATE.TWEEN
    # warning-ignore:return_value_discarded
    _tween.interpolate_property(
        self,
        '_tween_velocity',
        position.direction_to(_sort_target) * sort_speed,
        Vector2.UP * fly_speed,
        0.5,
        Tween.TRANS_CUBIC,
        Tween.EASE_IN
    )
    # warning-ignore:return_value_discarded
    _tween.interpolate_property(
        self,
        '_tween_rotation',
        _current_rotation_speed,
        0.0,
        3.0,
        Tween.TRANS_CUBIC,
        Tween.EASE_IN
    )
    # warning-ignore:return_value_discarded
    _tween.interpolate_property(
        self,
        'modulate',
        modulate,
        Color(0, 0, 0, 0),
        1.0,
        Tween.TRANS_CUBIC,
        Tween.EASE_IN,
        2.0
    )
    # warning-ignore:return_value_discarded
    _tween.start()
    # warning-ignore:return_value_discarded
    _tween.connect('tween_all_completed', self, 'queue_free')
